<?php

/**
 * @file
 * Lookup and did base on the GUID from another imorter.
 */

$plugin = array(
  'form' => 'fieldable_panels_panes_feeds_lookup_panelizer_did_form',
  'callback' => 'fieldable_panels_panes_feeds_lookup_panelizer_did_callback',
  'name' => 'Lookup display id',
  'multi' => 'loop',
  'category' => 'Other',
);

function fieldable_panels_panes_feeds_lookup_panelizer_did_form($importer, $element_key, $settings) {
  $options = array();
  $importers = feeds_importer_load_all();
  $uniques = array();

  foreach ($importers as $id => $imp) {
    $options[$id] = $imp->config['name'];

    $mappings = $imp->processor->config['mappings'];
    foreach ($mappings as $mapping) {
      if ($mapping['unique']) {
        dpm($mapping);
      }
    }
  }

  $form = array();
  $form['importer'] = array(
    '#type' => 'select',
    '#title' => t('Importer'),
    '#default_value' => isset($settings['importer']) ? $settings['importer'] : NULL,
    '#options' => $options,
  );
  return $form;
}

function fieldable_panels_panes_feeds_lookup_panelizer_did_callback($result, $item_key, $element_key, &$field, $settings) {
  $guid = $result->items[$item_key]['file_path'];
  $args = array(':guid' => $guid, ':importer' => $settings['importer']);
  $entity = db_query("SELECT entity_id, entity_type FROM {feeds_item} WHERE guid = :guid AND id = :importer", $args)->fetchObject();
  $args = array(':etid' => $entity->entity_id, ':etype' => $entity->entity_type);
  $field = db_query("SELECT did FROM {panelizer_entity} WHERE entity_id = :etid AND entity_type = :etype", $args)->fetchField();
}
