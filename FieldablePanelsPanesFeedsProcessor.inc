<?php

/**
 * @file
 * Class definition of FieldablePanelsPanesFeedsProcessor.
 */

/**
 * Creates fieldable panels panes from feed items.
 */
class FieldablePanelsPanesFeedsProcessor extends FeedsProcessor {

  /**
   * Define entity type.
   */
  public function entityType() {
    return 'fieldable_panels_pane';
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Fieldable panels panes');
    return $info;
  }

  /**
   * Creates a new fieldable panels pane in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $values = array(
      'bundle' => $this->config['bundle'],
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'reusable' => $this->config['reusable'],
    );

    $entity = fieldable_panels_panes_create($values);

    if ($this->config['create_pane']) {
      $entity->pane = $this->newPanelsPane($source);
    }
    return $entity;
  }

  /**
   * Creates a new panels pane in memory and returns it.
   */
  protected function newPanelsPane(FeedsSource $source) {
    $pane = new stdClass();
    $pane->region = $this->config['region'];
    $pane->did = 0;
    $pane->panel = '';
    return $pane;
  }

  /**
   * Loads an existing fieldable panels pane.
   */
  protected function entityLoad(FeedsSource $source, $entity_id) {
    $entity = fieldable_panels_panes_load($entity_id);

    if ($this->config['create_pane']) {
      $args = array(':subtype' => "fpid:$entity_id");
      $entity->pane = db_query("SELECT * FROM {panels_pane} WHERE type = 'fieldable_panels_pane' AND subtype = :subtype", $args)->fetchObject();
      if (!$entity->pane) {
        $entity->pane = $this->newPanelsPane($source);
      }
    }

    return $entity;
  }

  /**
   * Save a fieldable panels pane.
   */
  public function entitySave($entity) {
    // Grab the panels pane if it exists.
    if (isset($entity->pane)) {
      $pane = $entity->pane;
      unset($entity->pane);
    }

    // Save the new entity first so that we have an fpid.
    fieldable_panels_panes_save($entity);

    // Save our panels pane.
    if ($this->config['create_pane'] && isset($pane) && !empty($entity->fpid)) {
      $pane->type = 'fieldable_panels_pane';
      $pane->subtype = 'fpid:' . $entity->fpid;

      if ($this->config['auto_pane_position'] && !isset($pane->position)) {
        $args = array(':did' => $pane->did, ':panel' => $pane->panel);
        $max = db_query("SELECT max(position) FROM {panels_pane} WHERE did = :did AND panel = :panel", $args)->fetchField();
        if ($max === FALSE) {
          $pane->position = 0;
        }
        else {
          $pane->position = ++$max;
        }
      }

      if (isset($pane->pid)) {
        db_delete('panels_pane')
          ->condition('pid', $pane->pid)
          ->execute();
      }

      drupal_write_record('panels_pane', $pane);
    }
  }

  /**
   * Delete a series of fieldable panels panes.
   */
  protected function entityDeleteMultiple($entity_ids) {
    fieldable_panels_panes_delete_multiple($entity_ids);

    if ($this->config['create_pane']) {
      $pids = array();
      foreach ($entity_ids as $id) {
        $pids[] = "fpid:$id";
      }
      if ($pids) {
        db_delete('panels_pane')
          ->condition('type', 'fieldable_panels_pane')
          ->condition('subtype', $pids)
          ->execute();
      }
    }
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $bundles = fieldable_panels_panes_get_bundle_labels();
    $bundle = !empty($bundles) ? key($bundles) : NULL;

    return array(
      'bundle' => $bundle,
      'reusable' => FALSE,
      'create_pane' => FALSE,
      'region' => '',
      'auto_pane_position' => FALSE,
    ) + parent::configDefaults();
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    // Hrm, Feeds assumes "node" in FeedsProcessor.
    // @todo Fix this in Feeds.
    $form['input_format']['#description'] = t('Select the input format for the textarea fields of the entity to be created.');

    $form['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Bundle'),
      '#description' => t('Select the bundle for the Fieldable panels panes to be created.', array('!feed_id' => $this->id)),
      '#options' => fieldable_panels_panes_get_bundle_labels(),
      '#default_value' => $this->config['bundle'],
      '#required' => TRUE,
    );

    $form['reusable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Reusable'),
      '#default_value' => $this->config['reusable'],
      '#description' => t('Allow this Fieldable panels pane to be reused.'),
    );

    $form['create_pane'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create pane'),
      '#default_value' => $this->config['create_pane'],
      '#description' => t('Create an accompanying Panels pane that will display the entity.'),
    );

    $form['region'] = array(
      '#type' => 'textfield',
      '#title' => t('Default panel region'),
      '#description' => t('The default region for this Panels pane. I.e. left, right, center. This can be overridden by mapping a value to the region target.'),
      '#default_value' => $this->config['region'],
      '#states' => array(
        'visible' => array(
          ':input[name="create_pane"]' => array(
            'checked' => TRUE,
          ),
        ),
      ),
    );

    $form['auto_pane_position'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto pane position'),
      '#description' => t('Automatically calculate the position of the Panels pane.'),
      '#default_value' => $this->config['auto_pane_position'],
      '#states' => array(
        'visible' => array(
          ':input[name="create_pane"]' => array(
            'checked' => TRUE,
          ),
        ),
      ),
    );

    return $form;
  }

  /**
   * Override setTargetElement to operate on a target item that is a fieldable
   * panels pane.
   */
  public function setTargetElement(FeedsSource $source, $target_entity, $target_element, $value) {
    switch ($target_element) {
      case 'created':
      case 'timestamp':
        $target_entity->$target_element = feeds_to_unixtime($value, REQUEST_TIME);
        break;
      case 'reusable':
        $target_entity->reusable = (int) $value;
        break;
      default:
        if ($this->config['create_pane'] && strpos($target_element, 'panels_pane:') === 0) {
          list( , $element) = explode(':', $target_element, 2);
          parent::setTargetElement($source, $target_entity->pane, $element, $value);
        }
        else {
          parent::setTargetElement($source, $target_entity, $target_element, $value);
        }
        break;
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();

    $targets['title'] = array(
      'name' => t('Title'),
      'description' => t('The title of the Fieldable panels pane.'),
      'optional_unique' => TRUE,
    );

    $targets['admin_title'] = array(
      'name' => t('Admin title'),
      'description' => t('The administration title of the Fieldable panels pane.'),
      'optional_unique' => TRUE,
    );

    $targets['fpid'] = array(
      'name' => t('FPID'),
      'description' => t('The fpid of the entity. NOTE: use this feature with care, ids are usually assigned by Drupal.'),
      'optional_unique' => TRUE,
    );

    $targets['admin_description'] = array(
      'name' => t('Admin description'),
      'description' => t('The administration description of the Fieldable panels pane.'),
    );

    $targets['category'] = array(
      'name' => t('Category'),
      'description' => t('The category of the Fieldable panels pane.'),
    );

    $targets['created'] = array(
      'name' => t('Created'),
      'description' => t('The date the Fieldable panels pane was created.'),
    );

    $targets['uuid'] = array(
      'name' => t('UUID'),
      'description' => t('The UUID of the Fieldable panels pane.'),
      'optional_unique' => TRUE,
    );

    if ($this->config['create_pane']) {

      $targets['panels_pane:pid'] = array(
        'name' => t('Pane: PID'),
        'description' => t('The pid of the Panels pane. NOTE: use this feature with care, ids are usually assigned by Drupal.'),
        'optional_unique' => TRUE,
      );

      $targets['panels_pane:did'] = array(
        'name' => t('Pane: DID'),
        'description' => t('The display id of the Panels pane. This <strong>must</strong> contain a value for the pane to be visible anywhere.'),
      );

      $targets['panels_pane:panel'] = array(
        'name' => t('Pane: Panel'),
        'description' => t('The panel this pane should go in. I.e. left, right, center.'),
      );

      $targets['panels_pane:position'] = array(
        'name' => t('Pane: Position'),
        'description' => t('The position of the Panels pane. Acts as a weight for multple panes in the same panel.'),
      );
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    feeds_alter('feeds_processor_targets', $targets, $this->entityType(), $this->config['bundle']);

    return $targets;
  }

  /**
   * Get the fpid of an existing feed entity if available.
   */
  protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
    if ($fpid = parent::existingEntityId($source, $result)) {
      return $fpid;
    }

    // Iterate through all unique targets and test whether they already exist
    // in the database.
    foreach ($this->uniqueTargets($source, $result) as $target => $value) {
      switch ($target) {
        case 'fpid':
        case 'title':
        case 'admin_title':
        case 'uuid':
          $fpid = db_query("SELECT fpid FROM {fieldable_panels_panes} WHERE $target = :value", array(':value' => $value))->fetchField();
          break;
      }
      if ($fpid) {
        // Return the first fpid found.
        return $fpid;
      }
    }
    return 0;
  }
}
